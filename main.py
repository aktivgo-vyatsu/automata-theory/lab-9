from polynomial.validation.validator import PolynomialValidator
from polynomial.validation.params import PolynomialValidatorParams

if __name__ == '__main__':
    params = PolynomialValidatorParams(
        numbers=['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
        signs=['+', '-'],
        comma='.'
    )

    validator = PolynomialValidator(params)

    polynomial = '-20x^2+1'

    try:
        # polynomial = input('enter polynomial: ')
        validator.validate(polynomial)
        print(polynomial, 'is correct')
    except Exception:
        print(polynomial, 'is not correct')
