from polynomial.validation.params import PolynomialValidatorParams


class PolynomialValidator:
    def __init__(self, params: PolynomialValidatorParams):
        self.params = params

    def validate(self, input: str):
        for i in range(len(input)):
            if input[i] in ['+', '-']:
                return self.__left_sign(input[i + 1:])
            if input[i] in self.params.comma:
                return self.__comma(input[i + 1:])
            if input[i] in self.params.numbers:
                return self.__left_num(input[i + 1:])
            if input[i] in 'x':
                return self.__x(input[i + 1:])
            raise Exception()

    def __left_sign(self, input: str):
        for i in range(len(input)):
            if input[i] in self.params.numbers:
                return self.__left_num(input[i + 1:])
            if input[i] in 'x':
                return self.__x(input[i + 1:])
            raise Exception()
        raise Exception()

    def __comma(self, input: str):
        for i in range(len(input)):
            if input[i] in self.params.numbers:
                return self.__right_num(input[i + 1:])
        raise Exception()

    def __left_num(self, input: str):
        for i in range(len(input)):
            if input[i] in self.params.numbers:
                continue
            if input[i] == self.params.comma:
                return self.__comma(input[i + 1:])
            if input[i] in 'x':
                return self.__x(input[i + 1:])
            if input[i] in ['+', '-']:
                return self.__right_sign(input[i + 1:])
            raise Exception()

    def __right_num(self, input: str):
        for i in range(len(input)):
            if input[i] in self.params.numbers:
                continue
            if input[i] in 'x':
                return self.__x(input[i + 1:])
            if input[i] in ['+', '-']:
                return self.__right_sign(input[i + 1:])
        raise Exception()

    def __x(self, input: str):
        for i in range(len(input)):
            if input[i] in '^':
                return self.__degree(input[i + 1:])
            if input[i] in ['+', '-']:
                return self.__right_sign(input[i + 1:])
            raise Exception()

    def __degree(self, input: str):
        for i in range(len(input)):
            if input[i] in self.params.numbers:
                return self.__degree_number(input[i + 1:])
            raise Exception()
        raise Exception()

    def __degree_number(self, input: str):
        for i in range(len(input)):
            if input[i] in self.params.numbers:
                continue
            if input[i] in ['+', '-']:
                return self.__right_sign(input[i + 1:])
            raise Exception()

    def __right_sign(self, input: str):
        for i in range(len(input)):
            if input[i] in self.params.numbers:
                return self.__left_num(input[i + 1:])
            if input[i] in 'x':
                return self.__x(input[i + 1:])
            raise Exception()
        raise Exception()