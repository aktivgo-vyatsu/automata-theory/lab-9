from polynomial.validation.validator import PolynomialValidator
from polynomial.validation.params import PolynomialValidatorParams

if __name__ == '__main__':
    params = PolynomialValidatorParams(
        numbers=['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
        signs=['+', '-'],
        comma='.'
    )

    validator = PolynomialValidator(params)

    with open('input/polynomials.txt', 'r') as f:
        for line in (l.rstrip() for l in f.readlines()):
            try:
                validator.validate(line)
                print(line, 'is correct')
            except Exception:
                print(line, 'is not correct')
